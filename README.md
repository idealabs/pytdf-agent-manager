## Installation

Follow the instructions at https://github.com/idealabs/pytdf-sample-agent to
install both the base agent all desired agents into a single virtual
environment.

Create a config.ini file at the `pytdf-agent-manager` root (git is configured
to ignore this file). A sample config file may look like:

        [auth]
        email : myemail@example.com
        password : mypassword

        [Markowitz Agent]
        id : 42
        path : my-markowitz-agent
        mu : 2

        [Markowitz Agent Extreme]
        id : 43
        path : my-markowitz-agent
        mu : 1024

        [Sample Agent]
        id : 54
        path : pytdf-sample-agent

This config file requires a section called `[auth]` that stores the user name
and password you use to log in to the TDF website. It also includes a section
for each agent to be managed. The agent sections use the following key-values:

* `id`: The id of the agent. This is found on the agent edit screen
    on the TDF webpage (note, you must first create an agent in a
    league through the web interface, and then you can connect a
    script)
* `path`: The location of the agent's source code on the hard disk,
    relative to the parent of the agent manager (recommendation:
    install the agent manager and all agents in sibling directories
    for simple paths)
* `url` (OPTIONAL): The url where the web interface of the TDF instance
    sits. You probably won't need this unless working off the
    development or another alternative TDF.
* anything else (OPTIONAL): Additional arguments to pass to the
    specific agent. These will be given as key word arguments to
    the `agent_factory()` function in the agent's main.py.

    One use case for these additional arguments would be an agent
    that can be parameterized. For example, the Markowitz and MAD
    agents can be parameterized by `mu`, which represents the risk
    aversion of the agent. With the agent manager, you only need to
    create one implementation of these agents, and then set different
    `mu` values in the `config.ini` script here (as shown in the
    example above). This will run the same agent implementation as
    two separate agents, differentiated by their parameters.

Note: in order to function, each agent must have a `main.py` script in its root
with an `agent_factory()` function implemented within. The `agent_factory()`
returns an object that inherits from `BaseAgent`.

Once the manager has been configured, create a crontab entry for the manager.
The process is identical to that explained at
https://github.com/idealabs/pytdf-sample-agent, simply point the crontab to
the `main.py` script in this project rather than to an agent.
