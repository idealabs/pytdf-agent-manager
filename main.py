import os
import sys
import configparser
from functools import partial
from multiprocessing import Pool

NJOBS = 4  # Number of cores on which the agent manager will run


def _run_section(section_name, config):
    """
    Executes an agent described by a given section.
    """
    if section_name == 'auth':
        return

    section = config[section_name]
    sub_config = {'TDFConnect': {
        'email': email,
        'password': password,
        'agent_id': section['id']
    }}
    kwargs = {option: section[option]
              for option in config.options(section_name)
              if option not in ['id', 'path', 'url']}

    print('Executing Agent ' + section_name)

    try:
        path = section['path']
        relpath = os.path.join(os.path.dirname(__file__), '..')
        abspath = os.path.abspath(relpath)

        sys.path.insert(0, abspath)
        m = __import__(path, globals(), locals(), ['main'])
        del sys.path[0]

        url = section.get('url', None)
        agent = m.main.agent_factory(sub_config, url, **kwargs)
        agent.execute()
        print('Success on agent ' + section_name)
    except Exception as e:
        print('Failed on agent ' + section_name)
        print(e)


if __name__ == '__main__':
    # Need to fetch an absolute path of the config file relative to the current
    # file so that an execution of this script from another location (such as
    # by a crontab) will not lose the loction of the config.
    config_loc = os.path.join(os.path.dirname(__file__), 'config.ini')

    config = configparser.ConfigParser()
    config.read(config_loc)

    # Execute all agents
    email = config['auth']['email']
    password = config['auth']['password']

    run_section = partial(_run_section, config=config)

    p = Pool(processes=int(config['auth'].get('njobs', NJOBS)))
    p.map(run_section, config.sections())
